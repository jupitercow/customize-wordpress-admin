<?php
/*
Plugin Name: Jupitercow
Plugin URI: https://bitbucket.org/jupitercow/jupitercow-plugin
Description: Customize Wordpress admin area.
Version: 0.1
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2013 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('jcow_customize_admin') ) :

add_action( 'init', array('jcow_customize_admin', 'init') );

class jcow_customize_admin
{
	/**
	 * Class prefix
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $prefix = __CLASS__;

	/**
	 * Site developer name
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $developer = "Jupitercow";

	/**
	 * URL for site developer
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $developer_url = "http://jupitercow.com/";

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function init()
	{
		// Remove stuff from admin bar
		add_action( 'wp_before_admin_bar_render', array(__CLASS__, 'customize_admin_bar') );

		// Customize TinyMCE
		add_filter( 'tiny_mce_before_init',       array(__CLASS__, 'customize_TinyMCE') );

		// Add some custom dashboard widgets
		add_action( 'wp_dashboard_setup',         array(__CLASS__, 'custom_dashboar_widgets') );

		// Remove default dashboard widgets
		add_action( 'admin_menu',                 array(__CLASS__, 'disable_default_dashboard_widgets') );

		// Remove comments from media posts
		add_filter( 'comments_open',              array(__CLASS__, 'filter_media_comment_status'), 10 , 2 );

		//Custom admin footer text
		add_filter( 'admin_footer_text',          array(__CLASS__, 'admin_footer') );

		// Customize login page from the theme
		add_action( 'login_enqueue_scripts',      array(__CLASS__, 'login_css'), 10 );
		add_filter( 'login_headerurl',            array(__CLASS__, 'login_url') );
		add_filter( 'login_headertitle',          array(__CLASS__, 'login_title') );

		// Add an admin favicon
		add_action( 'admin_head',                 array(__CLASS__, 'admin_favicon'), 11 );

		// Remove screen options from dashboard
		add_filter( 'screen_options_show_screen', array(__CLASS__, 'remove_screen_options') );

		// Customize dashboard welcome message
		add_action( 'welcome_panel',              array(__CLASS__, 'dashboard_welcome_cleanup') );

		// Replace "Howdy"
		add_filter( 'gettext',                    array(__CLASS__, 'replace_howdy'), 10, 3 );
	}

	/**
	 * Replace howdy in the admin bar
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string Modified welcome message.
	 */
	public static function replace_howdy( $translated, $text, $domain )
	{
		if ( false !== strpos($translated, "Howdy") )
		{
			$new_message = apply_filters( self::$prefix . '/howdy', "Welcome back" );
			return str_replace("Howdy", $new_message, $translated);
		}
		return $translated;
	}

	/**
	 * Add a developer favicon
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function admin_favicon()
	{
		?>
		<link rel="icon" href="<?php echo plugins_url( 'favicon.png', __FILE__ ); ?>">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/<?php echo plugins_url( 'favicon.ico', __FILE__ ); ?>">
		<![endif]-->
		<?php
	}

	/**
	 * Add custom dashboard widgets
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function login_css()
	{
		wp_enqueue_style( self::$prefix . '_login', get_template_directory_uri() . '/library/css/login.css', false );
	}

	/**
	 * Change the logo link from wordpress.org to the site home
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function login_url()
	{
		return home_url( '/' );
	}

	/**
	 * Change the alt text on the logo to site name
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function login_title()
	{
		return get_option('blogname');
	}

	/**
	 * Add custom dashboard widgets
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function admin_footer()
	{
		?>
		<span id="footer-thankyou">Developed by <a href="<?php echo self::$developer_url; ?>" target="_blank"><?php echo self::$developer; ?></a></span>. Built using <a href="http://wordpress.org/" target="_blank">WordPress</a>.
		<?php
	}

	/**
	 * Add custom dashboard widgets
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function custom_dashboar_widgets()
	{
		wp_add_dashboard_widget( 'instructions_dashboard_widget', 'Website Instructions', array(__CLASS__, 'instructions_dashboard_widget') );
	}

	/**
	 * Create a basic instructions area on the Dashboard
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function instructions_dashboard_widget()
	{
		?>
		<h2><?php echo apply_filters( self::$prefix . '/dashboard_instructions/title', __("Use the menus on the left to add and edit website content.") ); ?></h2>
		<h1><a class="button" href="<?php echo admin_url( 'admin.php?page=acf-options' ); ?>"><?php _e("Update your organization options"); ?></a></h1>
		<h1><a class="button" href="<?php echo admin_url( 'edit.php?post_type=acf' ); ?>"><?php _e("Customize site forms"); ?></a></h1>
		<?php /** /<p><?php echo apply_filters( self::$prefix . '/dashboard_instructions/content', __("If you need help, please read the instructions below.") ); ?></p>
		<?php /** /<h1><a class="button" href="<?php bloginfo('stylesheet_directory'); ?>/Website_Instructions.pdf" target="_blank">Website Instructions</a></h1>
		/**/
	}

	/**
	 * Remove top admin menu items
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function customize_admin_bar()
	{
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('wp-logo');
		$wp_admin_bar->remove_menu('about');
		$wp_admin_bar->remove_menu('wporg');
		$wp_admin_bar->remove_menu('documentation');
		$wp_admin_bar->remove_menu('support-forums');
		$wp_admin_bar->remove_menu('feedback');
		$wp_admin_bar->remove_menu('view-site');
		$wp_admin_bar->remove_menu('new-content');
		$wp_admin_bar->remove_menu('new-link');
		$wp_admin_bar->remove_menu('new-media');
		$wp_admin_bar->remove_menu('new-user');
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('wpseo-menu');
	}

	/**
	 * Customize TinyMCE editor items.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	array Modified arguments for TinyMCE
	 */
	public static function customize_TinyMCE( $args )
	{
		$args['theme_advanced_blockformats'] = 'p,h1,h2,h3,h4,h5';
		$args['theme_advanced_disable']      = 'underline,forecolor,justifyfull';
		// Unhide the kitchen sink
		$args['wordpress_adv_hidden']        = false;

		return $args;
	}

	/**
	 * Disable default dashboard widgets.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function disable_default_dashboard_widgets()
	{
		remove_meta_box('dashboard_right_now', 'dashboard', 'core');       // Right Now Widget
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
		remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
		remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget
		remove_meta_box('dashboard_quick_press', 'dashboard', 'core');     // Quick Press Widget
		remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
		remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
		remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //
		
		// removing plugin dashboard boxes
		remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget
		remove_meta_box('tribe_dashboard_widget', 'dashboard', 'normal');  // Modern Tribe Plugin Widget
	}

	/**
	 * Turn off comments on media posts
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	bool Modified status for comments.
	 */
	public static function filter_media_comment_status( $open, $post_id )
	{
		$post = get_post( $post_id );
		if ( 'attachment' == $post->post_type ) return false;

		return $open;
	}

	/**
	 * Remove screen options from the dashboard
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	bool
	 */
	public static function remove_screen_options()
	{
		global $pagenow;

		if ( 'index.php' == $pagenow ) return false;
		return true;
	}

	/**
	 * Remove some screen options from the dashboard
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function dashboard_welcome_cleanup()
	{
		#global $pagenow;

		#if ( 'index.php' == $pagenow )
		#{
			?>
			<style type="text/css">
				.welcome-panel-column h4,
				.welcome-panel-last,
				.hide-if-no-customize {display: none !important;}
			</style>
			<?php
		#}
	}

	/**
	 * Add admin menu separator before or after ($mode) given menu item that is identified by slug
	 *
	 * Example: jcow_customize_admin::create_admin_menu_separator( 'edit.php?post_type=inquiry' );
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param 	string $slug Admin menu item slug
	 * @param 	string $mode Can be 'before' or 'after', by default it is 'before'
	 * @return	void
	 */
	public static function create_admin_menu_separator( $slug, $mode='before' )
	{
		global $menu;

		$count = 0;
		foreach ( $menu as $section )
		{
			// Find given slug
			if ( $section[2] == $slug )
			{
				// Before or after
				if ( 'after' == $mode ) $count++;
				
				// Part of the menu before given slug
				$new_menu = array_slice($menu, 0, $count, true);
				
				// Add separator
				$new_menu[] = array( '', 'read', 'separator'. $count, '', 'wp-menu-separator' );
				
				// Part of the menu after given slug
				$after = array_slice($menu, $count, null, true);
				foreach ( $after as $aoffset => $asection )
				{
					$new_menu[$aoffset+1] = $asection;
				}
				
				// Overwrite old menu with our new menu
				$menu = $new_menu;
				break;
			}
			$count++;
		}
	}
}

endif;